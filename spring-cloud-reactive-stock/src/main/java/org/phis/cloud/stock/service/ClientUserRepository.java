package org.phis.cloud.stock.service;

import org.phis.cloud.stock.entity.ClientUser;
import org.springframework.data.r2dbc.repository.R2dbcRepository;

public interface ClientUserRepository extends R2dbcRepository<ClientUser,String> {
    
}
