package org.phis.cloud.stock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class PhisStockApp {
	public static void main(String[] args) {
		SpringApplication.run(PhisStockApp.class, args);
	}
}
