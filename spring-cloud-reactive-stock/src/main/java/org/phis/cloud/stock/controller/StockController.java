package org.phis.cloud.stock.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.phis.cloud.stock.entity.ClientUser;
import org.phis.cloud.stock.service.ClientUserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("stock")
@RefreshScope
public class StockController {
	
	private static final Logger log = LoggerFactory.getLogger(StockController.class);
	
	@Value("${server.port}")
	String port;
	
	@Autowired
	ClientUserRepository clientUserRepository;

	@GetMapping("/reduce/{product}")
	public Mono<String> reduct(@PathVariable String product){
		log.info("扣减库存: " + product);
		
		ClientUser clientUser = new ClientUser();

		clientUser.setGender(2);
		clientUser.setNickName("r2dbc");
		clientUser.setPhoneNumber("9527");
		clientUser.setBirthday(LocalDate.of(1987, 11, 20));
		clientUser.setCtime(LocalDateTime.now());
		clientUser.setUtime(LocalDateTime.now());

		Mono<ClientUser> save = clientUserRepository.save(clientUser);
		
		save.subscribe(user -> System.out.println(user.getUserId()));
		
		return Mono.just("扣减库存: " + product + ", port: " + port);
	}
}
