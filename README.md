# spring-cloud-reactive

#### 介绍

spring-cloud-reactive是基于Spring Webflux的全异步微服务项目，采用SpringCloud、SpringCloudAlibaba相关组件构建。

#### 软件架构

软件架构说明



#### 技术选型

- 基础分布式服务套件：Spring Cloud &Spring Cloud Alibaba
- 服务注册与发现：Nacos
- 分布式统一配置中心：Nacos
- 网关：Spring Cloud Gateway
- 服务调用：Spring Cloud OpenFeign
- 负载均衡：Spring Cloud LoadBalancer
- 限流、降级：Sentinel
