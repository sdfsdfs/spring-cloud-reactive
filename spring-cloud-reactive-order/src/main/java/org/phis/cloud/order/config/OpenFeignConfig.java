package org.phis.cloud.order.config;

import org.phis.cloud.order.service.StockServiceFallback;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import feign.Logger;

@Configuration
public class OpenFeignConfig {

	@Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }
	
	@Bean
	public StockServiceFallback stockServiceFallback() {
		return new StockServiceFallback();
	}
}
