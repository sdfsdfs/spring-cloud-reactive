package org.phis.cloud.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.alicp.jetcache.anno.config.EnableCreateCacheAnnotation;

import reactivefeign.spring.config.EnableReactiveFeignClients;

@SpringBootApplication
@EnableReactiveFeignClients
@EnableCreateCacheAnnotation
public class PhisOrderApp {
	public static void main(String[] args) {
		SpringApplication.run(PhisOrderApp.class, args);
	}
}
