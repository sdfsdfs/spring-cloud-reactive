package org.phis.cloud.order.service;

import org.springframework.stereotype.Service;

@Service
public class EchoService {

    public String echo(String message) {
        return "[echo] Hello, " + message;
    }
}