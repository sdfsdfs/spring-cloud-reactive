package org.phis.cloud.order.service;

import org.phis.cloud.order.config.OpenFeignConfig;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import reactivefeign.spring.config.ReactiveFeignClient;
import reactor.core.publisher.Mono;

@ReactiveFeignClient(name = "phis-cloud-stock-service", 
	path = "/stock", 
	qualifier = "stockService", 
	fallback = StockServiceFallback.class, 
	configuration = OpenFeignConfig.class)
public interface StockService {

	@RequestMapping("/reduce/{product}")
	public Mono<String> reduct(@PathVariable("product") String product);
}
