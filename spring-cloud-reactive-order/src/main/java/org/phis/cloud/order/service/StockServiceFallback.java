package org.phis.cloud.order.service;

import reactor.core.publisher.Mono;

public class StockServiceFallback implements StockService {

	@Override
	public Mono<String> reduct(String product) {
		return Mono.just("StockService Fallback");
	}

}
