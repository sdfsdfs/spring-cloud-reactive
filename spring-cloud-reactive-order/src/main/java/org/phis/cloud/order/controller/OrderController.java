package org.phis.cloud.order.controller;

import javax.annotation.Resource;

import org.phis.cloud.order.service.StockService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

/**
 * @author chgz
 * @date 2021-12-29 09:16
 */
@RestController
@RequestMapping("order")
public class OrderController {
	
	@Resource(name = "stockService")
	StockService stockService;
	
    @GetMapping("create")
    public Mono<String> create(String product){
    	
    	Mono<String> stockMono = stockService.reduct(product);
    	
    	//stockMono.subscribe(s -> System.out.println(s));
    	
        return stockMono;
    }
}
