package org.gitee.cloud.gateway;

import java.time.ZonedDateTime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCloudGatewayApp {
	public static void main(String[] args) {
		System.out.println(ZonedDateTime.now());
		
		SpringApplication.run(SpringCloudGatewayApp.class, args);
		
	}
}
